﻿using System;
namespace RealeMutuaSsoIntegration.Request
{
    public class RequestData
    {
        public string Data { get; set; }
        public string Device { get; set; }
        public string CodiceApplicazione { get; set; }
        public string Compagnia { get; set; }
        public string Key { get; set; }

        public RequestData()
        {
            
        }
    }
        
}

