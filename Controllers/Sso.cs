﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RealeMutuaSsoIntegration.Request;
using RestSharp;

namespace RealeMutuaSsoIntegration.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SsoController : ControllerBase
    {
        private readonly ILogger<SsoController> _logger;
        private readonly IHttpClientFactory _clientFactory;

        private readonly IConfiguration _configuration;

        public SsoController(ILogger<SsoController> logger, 
                                IHttpClientFactory clientFactory,
                                IConfiguration configuration)
        {
            _clientFactory = clientFactory;
            _logger = logger;   
            _configuration = configuration;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromForm] string cryptData)
        {
            var internalAuthErrorUrl = _configuration["AppSettings:InternalAuthErrorUrl"];
            try
            {
                var externalAuthUrl = _configuration["AppSettings:ExteranlAuthUrl"];
                RequestData requestData = new RequestData();
                requestData.Data = cryptData;
                requestData.Device = _configuration["AppSettings:Device"];
                requestData.CodiceApplicazione = _configuration["AppSettings:CodiceApplicazione"];
                requestData.Compagnia = _configuration["AppSettings:Compagnia"];
                requestData.Key = _configuration["AppSettings:Key"];
                string json = JsonConvert.SerializeObject(requestData,Formatting.Indented);

                var client = new RestClient(externalAuthUrl);
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("x-Gateway-APIKey", _configuration["AppSettings:x-Gateway-APIKey"]);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", json, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                //Console.WriteLine(response.Content);

                DecryptModel jsonResult = JsonConvert.DeserializeObject<DecryptModel>(response.Content);  //JObject.Parse<DecryptModel>(response.Content);

                Result utenteResult = JsonConvert.DeserializeObject<Result>(jsonResult.Result);

                if (!string.IsNullOrEmpty(jsonResult.Exception))
                {
                    throw new ApplicationException(jsonResult.Exception);
                }

                var codiceUtente = utenteResult.codUtente;
                var redSection = utenteResult.redSection;
                var internalAuthUrl = _configuration["AppSettings:InternalAuthUrl"];

                return Redirect(string.Format("{0}{1}{2}", internalAuthUrl, codiceUtente, redSection));
            }
            catch (System.Exception e)
            {
                _logger.LogInformation("Si è verificato un errore imprevisto durante la chiamata al servizio esterno: " + e.Message);
                return Redirect(string.Format("{0}{1}", internalAuthErrorUrl));
            }
        }

        [HttpGet]
        public IActionResult Get()
        {
            _logger.LogInformation("Redirect " + _configuration["AppSettings:ExteranlAuthUrl"]);
            return Redirect("https://google.com");
        }
    }
}
